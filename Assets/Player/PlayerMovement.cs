using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerMovement : MonoBehaviour
{

    [Header("Stats")]
    public float speedHorz;
    public float speedVert;

    [Header("Fill-ins")]
    public GameObject snowboard;
    public GameObject sphereGuide;
    public LayerMask layers;

    Rigidbody sphereGuideRB;
    RaycastHit rayHitInfo;
    Rigidbody rigidBody;


/* to get tangent, take deriv of normal, and normalize it */
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        sphereGuideRB = sphereGuide.GetComponent<Rigidbody>();
        DOTween.Init();
    }

    void Update()
    {
        // get input
        float horz = Input.GetAxisRaw("Horizontal");
        float vert = Input.GetAxisRaw("Vertical");

        // update position based on sphere guide
        Vector3 spherePos = sphereGuide.transform.position;
        spherePos.y -= 0.13f; // magic number to get the board closer to the ground
        transform.position = spherePos;
        // get sphere's normalized velocity
        Vector3 normVelocity = sphereGuideRB.velocity.normalized;


        // Update rotation based on normal of mesh below us
        if (Physics.Raycast(snowboard.transform.position, -snowboard.transform.up, out rayHitInfo, 3.0f, layers)) {
            DrawArrow.ForDebug(snowboard.transform.position, -snowboard.transform.up*5.0f, Color.red);
            transform.DOLookAt(transform.position + (normVelocity * 2.0f), 0.2f, AxisConstraint.None, rayHitInfo.normal);
            //transform.LookAt(transform.position + (normVelocity * 2.0f), rayHitInfo.normal);
        }

        if (vert > 0.0f) {
            sphereGuideRB.AddForce(snowboard.transform.forward * speedVert, ForceMode.Acceleration);
        }
        else if (vert < 0.0f && sphereGuideRB.velocity.magnitude > 1.0f) {
            sphereGuideRB.AddForce(-snowboard.transform.forward * speedVert, ForceMode.Acceleration);
        }

        if (horz > 0.0f) {
            sphereGuideRB.AddForce(snowboard.transform.right * speedHorz, ForceMode.Acceleration);
        }
        else if (horz < 0.0f) {
            sphereGuideRB.AddForce(-snowboard.transform.right * speedHorz, ForceMode.Acceleration);
        }

    }

}
